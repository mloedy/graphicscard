public class Main {
    public static void main(String[] args) {
        GraphicCard graphicCard = new GraphicCard();
        graphicCard.setGraphicsSettings(new HDSetting());
        graphicCard.graphicsSettings();
        System.out.println(graphicCard.NeededPower());
        graphicCard.setGraphicsSettings(new MediumSetting());
        graphicCard.graphicsSettings();
        System.out.println(graphicCard.NeededPower());
        graphicCard.setGraphicsSettings(new LowSetting());
        graphicCard.graphicsSettings();
        System.out.println(graphicCard.NeededPower());

    }
}
