public class LowSetting implements IGraphicSettings {


    @Override
    public int getNeededProcessingPower() {
        return 100;
    }

    @Override
    public void processFrame() {

    }
}
