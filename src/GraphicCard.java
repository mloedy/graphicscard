public class GraphicCard {
    private IGraphicSettings GraphicsSettings;


    public void setGraphicsSettings(IGraphicSettings graphicsSettings) {
        GraphicsSettings = graphicsSettings;
    }

    public void graphicsSettings() {
        GraphicsSettings.getNeededProcessingPower();
    }

    public int NeededPower() {
        return GraphicsSettings.getNeededProcessingPower();
    }
}
